KDIR	:= /lib/modules/$(shell uname -r)/build
PWD	= $(shell pwd)

obj-m := usbnet.o asix.o ax88179_178a.o 
asix-y := asix_common.o  asix_devices.o  ax88172a.o
EXTRA_CFLAGS = -fno-pie
TOOL_EXTRA_CFLAGS = -Werror

MDIR	= kernel/drivers/net/usb

all:
	make -C $(KDIR) M=$(PWD) modules
	
install:
	make -C $(KDIR) M=$(PWD) modules_install
	depmod -a
	
clean:
	make -C $(KDIR) M=$(PWD) clean
