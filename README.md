# 编译方法
```sh
make
sudo make install
```

# 手动加载方法
```sh
sudo modprobe mii
sudo modprobe phylink
sudo modprobe usbnet
sudo modprobe asix
sudo modprobe ax88179_178a
```
